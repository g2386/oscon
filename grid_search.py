import os

from pose.dataset import PoseDataset, wrap as pose_wrap
from gest.dataset import GestureDataset, wrap as gest_wrap
from shared.selection import config_generator, stringify_config, run_experiment, save_run


def select_pose_architecture():
    architectures = [(63, 3), (63, 50, 3), (63, 200, 3), (63, 400, 3),
                     (63, 50, 50, 3), (63, 100, 100, 3)]
    lr = [1e-3, 1e-2, 1e-1]
    bs = [16, 64, 256]

    _configs = {'arch': architectures, 'lr': lr, 'bs': bs}
    configs = config_generator(_configs)

    train_ds = PoseDataset("data/pose-train.csv")
    valid_ds = PoseDataset("data/pose-valid.csv")
    test_ds = PoseDataset("data/pose-test.csv")

    for cfg_i, cfg in enumerate(configs):
        cfg_name = stringify_config(cfg, ['lr', 'bs'], ['arch'])
        cfg_path = f"data/stats/select_pose/{cfg_name}"
        print(f"Testing config {cfg_i+1}: {cfg_name}")
        if not os.path.exists(cfg_path):
            os.mkdir(cfg_path)
        for j in range(3):
            mod, losses, metrics = run_experiment(cfg, (train_ds, valid_ds, test_ds), 3, pose_wrap)
            save_run(losses, metrics, cfg_path, mod, j+1)


def select_gest_architecture():
    encoder_arch = [(1, 50), (1, 200), (2, 50), (2, 100)]
    decoder_arch = [[8], [50, 8], [100, 8]]
    lr = [1e-5, 5e-5, 1e-4]
    bs = [2, 4, 8]

    _configs = {'enc': encoder_arch, 'dec': decoder_arch, 'lr': lr, 'bs': bs}
    configs = config_generator(_configs)

    train_ds = GestureDataset("data/gest-train.csv")
    valid_ds = GestureDataset("data/gest-valid.csv")
    test_ds = GestureDataset("data/gest-test.csv")

    for cfg_i, cfg in enumerate(configs):
        cfg_name = stringify_config(cfg, ['lr', 'bs'], ['enc', 'dec'])
        cfg_path = f"./data/stats/select_gest/{cfg_name}"
        print(f"Testing config {cfg_i+1}: {cfg_name}")
        if not os.path.exists(cfg_path):
            os.mkdir(cfg_path)
        for j in range(3):
            mod, losses, metrics = run_experiment(cfg, (train_ds, valid_ds, test_ds), 8, gest_wrap)
            save_run(losses, metrics, cfg_path, mod, j+1)


def optimize_pose_hyperparams(arch):
    pat = [5, 10]
    b1 = [0.8, 0.9, 0.95]
    b2 = [0.99, 0.995, 0.999]
    wd = [1e-4, 5e-2]
    lr = [1e-4, 1e-3, 1e-2]
    bs = [64, 128]

    keys = ['pat', 'b1', 'b2', 'wd', 'lr', 'bs']
    _configs = {'pat': pat, 'b1': b1, 'b2': b2, 'wd': wd, 'lr': lr, 'bs': bs}
    configs = config_generator(_configs)

    train_ds = PoseDataset("data/pose-train.csv")
    valid_ds = PoseDataset("data/pose-valid.csv")
    test_ds = PoseDataset("data/pose-test.csv")

    for cfg_i, cfg in enumerate(configs):
        cfg_name = '_'.join([str(cfg[k]) for k in keys])
        cfg_path = f"./data/stats/hopt_pose/{cfg_name}"
        print(f"Testing config {cfg_i+1}: {cfg_name}")
        if not os.path.exists(cfg_path):
            os.mkdir(cfg_path)
        cfg['arch'] = arch
        mod, losses, metrics = run_experiment(cfg, (train_ds, valid_ds, test_ds), 3, pose_wrap)
        save_run(losses, metrics, cfg_path, mod)


def optimize_gest_hyperparams(enc, dec):
    pat = [5, 10]
    b1 = [0.8, 0.9, 0.95]
    b2 = [0.99, 0.995, 0.999]
    wd = [1e-4, 5e-2]
    lr = [5e-5, 1e-4, 5e-4]
    bs = [4, 8]

    train_ds = GestureDataset("data/gest-train.csv")
    valid_ds = GestureDataset("data/gest-valid.csv")
    test_ds = GestureDataset("data/gest-test.csv")

    _configs = {'pat': pat, 'b1': b1, 'b2': b2, 'wd': wd, 'lr': lr, 'bs': bs}
    configs = config_generator(_configs)
    keys = ['pat', 'b1', 'b2', 'wd', 'lr', 'bs']

    for cfg_i, cfg in enumerate(configs):
        cfg_name = '_'.join([str(cfg[k]) for k in keys])
        cfg_path = f"./data/stats/hopt_gest/{cfg_name}"
        print(f"Testing config {cfg_i+1}: {cfg_name}")
        if not os.path.exists(cfg_path):
            os.mkdir(cfg_path)
        cfg['enc'] = enc
        cfg['dec'] = dec
        mod, losses, metrics = run_experiment(cfg, (train_ds, valid_ds, test_ds), 8, gest_wrap)
        save_run(losses, metrics, cfg_path, mod)


if __name__ == '__main__':
    select_pose_architecture()
    select_gest_architecture()
    optimize_pose_hyperparams([63, 3])
    optimize_gest_hyperparams([1, 100], [8])
