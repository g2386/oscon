import tkinter
import mouse
import keyboard

root = tkinter.Tk()
SCREEN_WIDTH = root.winfo_screenwidth()
SCREEN_HEIGHT = root.winfo_screenheight()
root.destroy()


def perform_action(gesture):
    if gesture == 0:
        mouse.press(mouse.LEFT)
    elif gesture == 1:
        mouse.release(mouse.LEFT)
    elif gesture == 2:
        mouse.click(mouse.LEFT)
    elif gesture == 3:
        mouse.double_click(mouse.LEFT)
    elif gesture == 4:
        mouse.click(mouse.RIGHT)
    elif gesture == 5:
        keyboard.press('ctrl')
        mouse.wheel(1)
        keyboard.release('ctrl')
    elif gesture == 6:
        keyboard.press('ctrl')
        mouse.wheel(-1)
        keyboard.release('ctrl')


def move(position, absolute=True):
    mouse.move(int(position[0] * SCREEN_WIDTH), int(position[1] * SCREEN_HEIGHT), absolute=absolute)


def scroll(steps):
    mouse.wheel(steps)
