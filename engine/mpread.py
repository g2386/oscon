import numpy as np
import cv2


def frames(hands, cap):
    while cap.isOpened():
        success, img = cap.read()
        if not success:
            yield None
            continue

        img.flags.writeable = False
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        results = hands.process(img)

        if not results.multi_hand_landmarks:
            yield None
            continue

        landmarks = next(iter(results.multi_hand_landmarks)).landmark
        X = np.array([[getattr(lm, c) for lm in landmarks] for c in "xyz"])
        X[0, :] = 1 - X[0, :]
        yield X
