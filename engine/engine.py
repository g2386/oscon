import numpy as np
import torch

import json

from engine.actions import perform_action, move, scroll

S_PTR, S_SCR, S_STP = 0, 1, 2
S_MOV, S_FRZ, S_COL = 0, 1, 2
W, T1, T2, T3, TT, I1, I2, I3, IT, M1, M2, M3, MT, R1, R2, R3, RT, P1, P2, P3, PT = range(21)


class Tick:
    def __init__(self, count):
        self._count = count
        self.current = count

    def tick(self):
        self.current -= 1
        if self.current == 0:
            self.reset()
            return True
        return False

    def reset(self):
        self.current = self._count


class History:
    def __init__(self, dims):
        self.size = dims[0]
        self.dims = dims
        self.history = np.zeros(dims)
        self.start = 0
        self.full = False

    def push(self, frame):
        self.history[self.start] = frame
        self.start += 1
        if self.start == self.size:
            self.full = True
            self.start = 0

    def last(self, count=1, lm_idx=None):
        if count < 0 or count > len(self):
            raise IndexError

        arr = self[-count:len(self)]
        if lm_idx is not None:
            arr = arr[:, [lm_idx + 21*i for i in range(3)]]
        return arr

    def reset(self):
        self.start = 0
        self.full = False
        self.history = np.zeros(self.dims)

    def __len__(self):
        return self.size if self.full else self.start

    def __getitem__(self, item):
        true_len = len(self)
        if isinstance(item, slice):
            start, end = item.start, item.stop

            start = 0 if start is None else start
            end = true_len if end is None else end

            if start < 0:
                start += true_len
            if end <= 0:
                end += true_len

            if start < 0 or start >= true_len or end <= 0 or end > true_len:
                raise IndexError

            start_idx = (start + self.start) % self.size
            end_idx = (end + self.start) % self.size
            if end_idx > start_idx:
                return self.history[start_idx:end_idx]

            if end_idx == 0:
                return self.history[start_idx:]

            return np.concatenate((
                self.history[start_idx:],
                self.history[:end_idx]
            ))

        elif isinstance(item, int):
            if item < 0:
                item = true_len + item

            if item >= true_len or item < 0:
                raise IndexError

            idx = (item + self.start) % self.size
            return self.history[idx]
        else:
            raise IndexError


def normalize_frame(X):
    m, M = np.min(X, axis=1, keepdims=True), np.max(X, axis=1, keepdims=True)
    norm = (X - m) / (M - m)
    return norm


class OSCON:
    STATES = [S_PTR, S_SCR, S_STP]
    SUBSTATES = [S_MOV, S_FRZ, S_COL]

    def __init__(self, pose_clf, gest_clf, config):
        self.cfg = config
        self.cfg['min_pos'] = np.array([self.cfg['border_left'], self.cfg['border_top']])
        self.cfg['max_pos'] = np.array([self.cfg['border_right'], self.cfg['border_bottom']])
        self.cfg['area_dim'] = self.cfg['max_pos'] - self.cfg['min_pos']
        self.cfg['stat_area'] = np.array([self.cfg['freeze_width'], self.cfg['freeze_height']])

        self.pose_clf = pose_clf
        self.gest_clf = gest_clf

        self.history = History((self.cfg['history'], 63))
        self.norm_history = History((self.cfg['history'], 63))
        self.pose_tick = Tick(self.cfg['pose_int'])

        self.state = S_PTR
        self.substate = S_MOV
        self.state_processors = [self.process_pointer_state, self.process_scroll_state, self.process_stop_state]
        self.substate_processors = [self.process_mov_substate, self.process_frz_substate, self.process_col_substate]

        # PTR state management
        self.stat_tick = Tick(self.cfg['freeze_int'])
        self.stat_loc = None
        self.detect_tick = Tick(self.cfg['detect_int'])
        self.frame_count = 0

        # SCR state management
        self.scroll_start = None
        self.scroll_last = None
        self.scrolled = 0

        # STP state management
        self.count_stops = 0

    def feed(self, frame):
        if frame is None:
            return True

        norm_flat = self.update_history(frame)
        if len(self.history) < self.cfg['wait_frames']:
            return True

        if self.pose_tick.tick() and not (self.state == S_PTR and self.substate in [S_FRZ, S_COL]):
            gesture = self.classify_pose(norm_flat)
            self.update_state(gesture)

        state_processor = self.state_processors[self.state]
        return state_processor()

    def update_history(self, frame):
        flat = frame.flatten()
        norm_flat = normalize_frame(frame).flatten()
        self.history.push(flat)
        self.norm_history.push(norm_flat)
        return norm_flat

    def update_state(self, state):
        if state == self.state:
            return

        self.state = state
        self.substate = S_MOV

        if state == S_PTR:
            self.stat_tick.reset()
            self.stat_loc = None
            self.detect_tick.reset()
            self.frame_count = 0
        elif state == S_SCR:
            self.scroll_start = self.history.last(lm_idx=I1).flatten()[1]
            self.scroll_last = self.history.last(lm_idx=I1).flatten()[1]
            self.scrolled = 0
        elif state == S_STP:
            self.count_stops = 0

        print(f"State update: {self.state}")

    def process_pointer_state(self):
        substate_processor = self.substate_processors[self.substate]
        return substate_processor()

    def process_scroll_state(self):
        current = self.history.last(lm_idx=I1).flatten()[1]
        diff = current - self.scroll_start
        steps = int(diff / self.cfg['scroll_step'])
        delta = steps - self.scrolled
        moment = abs(current - self.scroll_last)

        factor = self.cfg['scroll_factor']
        if moment > self.cfg['scroll_moment3']:
            factor = self.cfg['scroll_factor3']
        elif moment > self.cfg['scroll_moment2']:
            factor = self.cfg['scroll_factor2']

        self.scrolled = steps
        self.scroll_last = current
        scroll(delta*factor)
        return True

    def process_stop_state(self):
        self.count_stops += 1
        return self.count_stops < self.cfg['min_stop']

    def process_mov_substate(self):
        avg_ptr = self.get_new_pointer_location()
        move(avg_ptr)

        if self.stat_tick.tick():
            X_stat = self.history.last(count=self.cfg['freeze_count'], lm_idx=W)[:, :2]  # 10x2
            sd_stat = np.average(np.std(X_stat, axis=0))
            if sd_stat < self.cfg['sd_to_freeze']:
                self.substate = S_FRZ
                self.stat_tick.reset()
                self.stat_loc = avg_ptr
                print(f"stddev(wrist) = {sd_stat} < {self.cfg['sd_to_freeze']}")
                print("POINTER.MOVE --> POINTER.FREEZE\n")
        return True

    def process_frz_substate(self):
        avg_ptr = self.get_new_pointer_location()
        diff = avg_ptr - self.stat_loc
        target = self.stat_loc + diff * self.cfg['stat_area']
        move(target)

        if self.detect_tick.tick():
            sd_max = self.get_max_distance_stddev()
            if sd_max > self.cfg['sd_start']:
                self.substate = S_COL
                self.detect_tick.reset()
                self.frame_count = 5
                print(f"max(stddev(distance)) = {sd_max} > {self.cfg['sd_start']}")
                print("POINTER.FREEZE --> POINTER.COLLECT\n")
                return True

        if self.stat_tick.tick():
            X_stat = self.history.last(count=self.cfg['freeze_count'], lm_idx=W)[:, :2]  # 5x2
            sd_stat = np.average(np.std(X_stat, axis=0))
            if sd_stat > self.cfg['sd_to_move']:
                self.substate = S_MOV
                self.stat_tick.reset()
                print(f"stddev(wrist) = {sd_stat} > {self.cfg['sd_to_move']}")
                print("POINTER.FREEZE --> POINTER.MOVE\n")
        return True

    def process_col_substate(self):
        self.frame_count += 1
        if self.detect_tick.tick():
            sd_max = self.get_max_distance_stddev()
            if sd_max < self.cfg['sd_end']:
                self.frame_count += 5
                gesture = self.norm_history.last(count=self.frame_count)
                clazz = self.classify_gesture(gesture)
                perform_action(clazz)

                self.substate = S_MOV
                self.detect_tick.reset()
                self.frame_count = 0

        if self.frame_count > self.cfg['max_frames']:
            self.substate = S_MOV
            self.detect_tick.reset()
            self.frame_count = 0

        return True

    def classify_pose(self, frame):
        pt_X = torch.from_numpy(frame).float().to(device=torch.device('cuda:0'))
        out = self.pose_clf.forward_single(pt_X)
        idx = torch.argmax(out)
        return idx

    def classify_gesture(self, frames):
        pt_X = torch.from_numpy(frames).float()
        out = self.gest_clf.forward_single(pt_X)
        idx = torch.argmax(out)
        return idx

    def get_new_pointer_location(self):
        X_ptr = self.history.last(count=self.cfg['pointer_count'], lm_idx=IT)[:, :2]  # 5x2
        avg_ptr = np.average(X_ptr, axis=0).flatten()
        avg_ptr = np.maximum(np.minimum(avg_ptr, self.cfg['max_pos']), self.cfg['min_pos'])
        avg_ptr = (avg_ptr - self.cfg['min_pos']) / self.cfg['area_dim']
        return avg_ptr

    def get_max_distance_stddev(self):
        thumb_tip = self.norm_history.last(count=self.cfg['detect_count'], lm_idx=TT)
        index_tip = self.norm_history.last(count=self.cfg['detect_count'], lm_idx=IT)
        middle_tip = self.norm_history.last(count=self.cfg['detect_count'], lm_idx=MT)
        middle_mid = self.norm_history.last(count=self.cfg['detect_count'], lm_idx=M2)
        sd_tt_m2 = np.std(np.sum(np.abs(thumb_tip - middle_mid), axis=1))
        sd_it_mt = np.std(np.sum(np.abs(index_tip - middle_tip), axis=1))
        sd_tt_mt = np.std(np.sum(np.abs(thumb_tip - middle_tip), axis=1))
        return np.max(np.array([sd_tt_m2, sd_it_mt, sd_tt_mt]))
