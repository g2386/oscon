import os
from itertools import product

import pandas as pd
import numpy as np

import torch
from torch.utils.data import Dataset, DataLoader

FEATURE_COLS = [f"{c}{n}" for c, n in product("xyz", range(0, 21))]
GESTURE_TO_CLASS = {'PTR': 0, 'SCR': 1, 'STP': 2}


def wrap(dataset, bs=64):
    return DataLoader(dataset, batch_size=bs, shuffle=True)


def class_to_onehot(y):
    Yoh = np.zeros((len(y), np.max(y) + 1))
    Yoh[range(len(y)), y] = 1
    return Yoh


class PoseDataset(Dataset):
    def __init__(self, path, device=torch.device('cuda:0')):
        df = pd.read_csv(path)
        X = df.drop('gesture', axis=1).to_numpy()
        Y = df['gesture'].map(GESTURE_TO_CLASS).to_numpy()
        Yoh = class_to_onehot(Y.astype('int'))
        self.X = torch.from_numpy(X).float().to(device=device)
        self.Y = torch.from_numpy(Yoh).to(device=device)

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        return self.X[idx], self.Y[idx]


if __name__ == '__main__':
    root = os.path.join(os.path.dirname(os.getcwd()), "data")
    train = PoseDataset(os.path.join(root, "pose-train.csv"))
    test = PoseDataset(os.path.join(root, "pose-test.csv"))
    for i in range(len(train)):
        print(train[i])
        if i == 20:
            break
