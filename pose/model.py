import torch
import torch.nn as nn

from pose.dataset import PoseDataset, wrap
from shared.training import train_loop


def create_layer(ldef):
    nntype = ldef['type']
    if nntype == 'fc':
        return nn.Linear(ldef['in'], ldef['out'], ldef.get('bias', True))
    elif nntype == 'relu':
        return nn.ReLU()
    else:
        raise ValueError("Unknown layer type")


class PoseClassifier(nn.Module):
    def __init__(self, layers, device=torch.device('cuda:0')):
        super().__init__()
        self.layers = nn.Sequential(
            *[create_layer(ldef) for ldef in layers]
        )
        self.dvc = device
        self.to(device=device)

    def forward(self, batch):
        h = batch[0]
        for layer in self.layers:
            h = layer.forward(h)
        return h

    def forward_single(self, x):
        h = x.to(device=self.dvc)
        for layer in self.layers:
            h = layer.forward(h)
        return h


if __name__ == '__main__':
    PATH = lambda x: f"../data/pose-{x}.csv"
    BATCH_SIZE = 64
    LR = 1e-3

    train = wrap(PoseDataset(PATH('train')), bs=BATCH_SIZE)
    test = wrap(PoseDataset(PATH('test')), bs=BATCH_SIZE)

    model = PoseClassifier([
        {'type': 'fc', 'in': 63, 'out': 200},
        {'type': 'relu'},
        {'type': 'fc', 'in': 200, 'out': 3}
    ])
    optimizer = torch.optim.Adam(model.parameters(), lr=LR)
    criterion = nn.CrossEntropyLoss()

    train_loop(model, (train, test), optimizer, criterion, 3)

    output_sd = model.state_dict()
    torch.save(output_sd, "../cp/pose/63-200-3.pt")
