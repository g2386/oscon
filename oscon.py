import os

import mediapipe as mp
import torch
import cv2

from pose.model import PoseClassifier
from gest.model import GestureClassifier
from engine.engine import OSCON
from engine.mpread import frames

from argparse import ArgumentParser
import json


def setup_arg_parser():
    parser = ArgumentParser()
    parser.add_argument("-g", "--config", type=str, default="./config.json",
                        help="path to configuration file")

    parser.add_argument("-y", "--history", type=int,
                        help="number of recent frames kept in history")

    parser.add_argument("-p", "--pose_int", type=int,
                        help="number of frames between pose classifications (updating states)")
    parser.add_argument("-f", "--freeze_int", type=int,
                        help="number of frames between estimations of hand stationarity")
    parser.add_argument("-d", "--detect_int", type=int,
                        help="number of frames between estimations of finger movement (dynamic gesture detection)")

    parser.add_argument("-l", "--pointer_count", type=int,
                        help="number of frames used to compute new pointer location")
    parser.add_argument("-c", "--freeze_count", type=int,
                        help="number of frames used to estimate hand stationarity")
    parser.add_argument("-n", "--detect_count", type=int,
                        help="number of frames used to detect gesture start and end")

    parser.add_argument("-w", "--wait_frames", type=int,
                        help="number of frames to wait before the system stabilizes and starts working normally")
    parser.add_argument("-s", "--min_stop", type=int,
                        help="minimal number of frames classified as STOP before OSCON shuts down")
    parser.add_argument("-m", "--max_frames", type=int,
                        help="maximal number of frames in a dynamic gesture")

    parser.add_argument("--border_left", type=float,
                        help="left border (in normalized camera space) of region used for pointer control")
    parser.add_argument("--border_right", type=float,
                        help="right border (in normalized camera space) of region used for pointer control")
    parser.add_argument("--border_top", type=float,
                        help="top border (in normalized camera space) of region used for pointer control")
    parser.add_argument("--border_bottom", type=float,
                        help="bottom border (in normalized camera space) of region used for pointer control")
    parser.add_argument("--freeze_height", type=float,
                        help="height of pointer control region when fine pointer control is activated")
    parser.add_argument("--freeze_width", type=float,
                        help="width of pointer control region when fine pointer control is activated")

    parser.add_argument("--sd_to_freeze", type=float,
                        help="hand stationarity threshold for updating state "
                             "from POINTER_MOVE updates state to POINTER_FREEZE")
    parser.add_argument("--sd_to_move", type=float,
                        help="hand stationarity threshold for updating state "
                             "from POINTER_FREEZE updates state to POINTER_MOVE")
    parser.add_argument("--sd_start", type=float,
                        help="finger stationarity threshold for gesture start detection")
    parser.add_argument("--sd_end", type=float,
                        help="finger stationarity threshold for gesture end detection")

    parser.add_argument("--scroll_step", type=float,
                        help="minimal vertical movement for one scroll (in fractions of height in camera space)")
    parser.add_argument("--scroll_factor", type=int,
                        help="default scroll count multiplication factor (scrolling speed level 1)")
    parser.add_argument("--scroll_factor2", type=int,
                        help="scroll count multiplication factor for scrolling speed level 2")
    parser.add_argument("--scroll_factor3", type=int,
                        help="scroll count multiplication factor for scrolling speed level 3")
    parser.add_argument("--scroll_moment2", type=float,
                        help="distance covered between two frames in SCROLL to warrant speed level 2")
    parser.add_argument("--scroll_moment3", type=float,
                        help="distance covered between two frames in SCROLL to warrant speed level 3")

    parser.add_argument("--pose_model", type=str,
                        help="directory containing definition and weights for the pose classification model")
    parser.add_argument("--pose_device", type=str, choices=['cpu', 'gpu'],
                        help="device on which the pose classification model should run")
    parser.add_argument("--gest_model", type=str,
                        help="directory containing definition and weights for the gesture classification model")
    parser.add_argument("--gest_device", type=str, choices=['cpu', 'gpu'],
                        help="device on which the gesture classification model should run")

    return parser


def load_config(path):
    with open(path) as f:
        cfg = json.load(f)
    return cfg


def override_config(config, args):
    for key in args.keys():
        config[key] = config[key] if args[key] is None else args[key]
    return config


def get_fc_layers(arch):
    fc_layers = [{'type': 'fc', 'in': arch[i], 'out': arch[i+1]} for i in range(len(arch)-1)]
    i = 1
    while i < len(fc_layers):
        fc_layers.insert(i, {'type': 'relu'})
        i += 2
    return fc_layers


def load_pose_model(path, device):
    files = os.listdir(path)
    files = list(filter(lambda f: f.endswith(".pt"), files))
    file = files[0]

    arch = [int(x) for x in file[:-3].split("-")]
    fc_layers = get_fc_layers(arch)

    model = PoseClassifier(fc_layers, device=torch.device('cpu' if device == 'cpu' else 'cuda:0'))
    model.load_state_dict(torch.load(f"{path}/{file}"))
    model.eval()
    return model


def load_gest_model(path, device):
    files = os.listdir(path)
    files = list(filter(lambda f: f.endswith(".pt"), files))
    file = files[0]

    arches = file[:-3].split("_")
    lstm_arch = [int(x) for x in arches[0].split("-")]
    fc_arch = [int(x) for x in arches[1].split("-")]
    lstm_opts = {'input_size': 63, 'num_layers': lstm_arch[0], 'hidden_size': lstm_arch[1]}
    fc_layers = get_fc_layers(fc_arch)

    model = GestureClassifier(lstm_opts, fc_layers, device=torch.device('cpu' if device == 'cpu' else 'cuda:0'))
    model.load_state_dict(torch.load(f"{path}/{file}"))
    model.eval()
    return model


def main():
    print("Starting OSCON...")
    parser = setup_arg_parser()
    args = vars(parser.parse_args())
    config = load_config(args['config'])
    config = override_config(config, args)

    print("1. Initializing connection to video stream...", end='')
    stream = cv2.VideoCapture(0)
    print("DONE")
    print("2. Initializing MediaPipe Hands...", end='')
    mp_hands = mp.solutions.hands
    print("DONE")
    print("3. Loading classifiers...", end='')
    pose_clx = load_pose_model(config['pose_model'], config['pose_device'])
    gest_clx = load_gest_model(config['gest_model'], config['pose_device'])
    print("DONE")
    print("4. Starting engine...", end='')
    engine = OSCON(pose_clx, gest_clx, config)
    print("DONE")

    with mp_hands.Hands(
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5
    ) as hands:
        print("5. Opening video stream...", end='')
        frame_generator = iter(frames(hands, stream))
        print("DONE")
        print("\nGesture control will begin shortly!")
        while True:
            #TODO: Take care of left-handedness
            #       - invert for classifier
            #       - dont invert for pointer
            status = engine.feed(next(frame_generator))
            if not status:
                stream.release()
                break

    print("Terminating OSCON...")


if __name__ == '__main__':
    main()
