import torch
import torch.nn as nn

from gest.dataset import GestureDataset, wrap
from shared.training import train_loop

DEVICE = torch.device('cuda:0')


def create_layer(ldef):
    nntype = ldef['type']
    if nntype == 'fc':
        return nn.Linear(ldef['in'], ldef['out'], ldef.get('bias', True))
    elif nntype == 'relu':
        return nn.ReLU()
    else:
        raise ValueError("Unknown layer type")


class GestureClassifier(nn.Module):
    def __init__(self, lstm_opts, dec_layers, device=torch.device('cuda:0')):
        super().__init__()
        self.cell = nn.LSTM(**lstm_opts)
        self.decoder = nn.Sequential(
            *[create_layer(layer) for layer in dec_layers]
        )
        self.dvc = device
        self.to(device=device)

    def forward(self, batch):
        x, _, indices = batch
        rec_out, rec_hn = self.cell(torch.transpose(x, 0, 1))
        output = rec_out[indices, [i for i in range(x.size(0))]]
        return self.decoder(output)

    def forward_single(self, x):
        x = x.to(device=self.dvc)
        rec_out, rec_hn = self.cell(x)
        output = rec_out[-1]
        return self.decoder(output)


def main():
    LSTM_OPTS = {
        'input_size': 63,
        'num_layers': 1,
        'hidden_size': 150
    }
    DEC_LAYERS = [
        {'type': 'fc', 'in': 150, 'out': 50},
        {'type': 'relu'},
        {'type': 'fc', 'in': 50, 'out': 8}
    ]
    LR = 1e-4
    EPOCHS = 80
    BATCH_SIZE = 2

    train = wrap(GestureDataset("../data/gest-train.csv", device=DEVICE), bs=BATCH_SIZE)
    test = wrap(GestureDataset("../data/gest-test.csv", device=DEVICE), bs=BATCH_SIZE)

    model = GestureClassifier(LSTM_OPTS, DEC_LAYERS)
    optimizer = torch.optim.Adam(model.parameters(), lr=LR)
    criterion = nn.CrossEntropyLoss()

    train_loop(model, (train, test), optimizer, criterion, 8, epochs=EPOCHS)

    output_sd = model.state_dict()
    torch.save(output_sd, "../cp/gest/1-150_150-50-8.pt")


if __name__ == '__main__':
    main()
