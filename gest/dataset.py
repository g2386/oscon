import os
from itertools import product

import pandas as pd
import numpy as np

import torch
from torch.utils.data import Dataset, DataLoader

FEATURE_COLS = [f"{c}{n}" for c, n in product("xyz", range(0, 21))]
GESTURE_TO_CLASS = {'PRESS': 0, 'RELEASE': 1, 'CLICK': 2, 'DOUBLE': 3, 'RIGHT': 4, 'ZIN': 5, 'ZOUT': 6, 'NONE': 7}


def wrap(dataset, bs=64):
    return DataLoader(dataset, batch_size=bs, shuffle=True, collate_fn=_collate_fn)


def class_to_onehot(y):
    Yoh = np.zeros((len(y), np.max(y) + 1))
    Yoh[range(len(y)), y] = 1
    return Yoh


def _collate_fn(batch):
    gestures, labels = zip(*batch)
    indices = torch.tensor([torch.argmin(torch.sum(g, dim=1)) for g in gestures]) - 1
    return torch.stack(gestures), torch.stack(labels), indices


class GestureDataset(Dataset):
    def __init__(self, path, device=torch.device('cuda:0')):
        df = pd.read_csv(path)
        X = df.drop('gesture', axis=1).to_numpy()
        X = np.expand_dims(X, axis=1)
        X = X.reshape((len(X), -1, 63))
        Y = df['gesture'].map(GESTURE_TO_CLASS).to_numpy()
        Yoh = class_to_onehot(Y.astype(int))
        self.X = torch.from_numpy(X).float().to(device=device)
        self.Y = torch.from_numpy(Yoh).to(device=device)

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        return self.X[idx], self.Y[idx]


if __name__ == '__main__':
    root = os.path.join(os.path.dirname(os.getcwd()), "data")
    train = GestureDataset(os.path.join(root, "gest-train.csv"))
    test = GestureDataset(os.path.join(root, "gest-test.csv"))

    train_dl = wrap(train, bs=10)
    for i, (x, y, l) in enumerate(train_dl):
        if i == 10:
            break