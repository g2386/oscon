from torch.optim import Adam
from pose.model import PoseClassifier
from gest.model import GestureClassifier


def create_ldef(arch):
    layers = []
    for i in range(len(arch) - 1):
        layers.append({'type': 'fc', 'in': arch[i], 'out': arch[i + 1], 'bias': True})
        if i < len(arch) - 2:
            layers.append({'type': 'relu'})
    return layers


def create_lstm_opts(arch):
    return {
        'input_size': 63,
        'num_layers': arch[0],
        'hidden_size': arch[1]
    }


def get_model(config):
    if 'arch' in config.keys():
        ldef = create_ldef(config['arch'])
        return PoseClassifier(ldef)
    elif 'enc' in config.keys() and 'dec' in config.keys():
        lstm_opts = create_lstm_opts(config['enc'])
        ldef = create_ldef([config['enc'][-1]] + config['dec'])
        return GestureClassifier(lstm_opts, ldef)


def parse_config(config):
    lr = config['lr']
    wd = config.get('wd', 0)
    b1 = config.get('b1', 0.99)
    b2 = config.get('b2', 0.999)

    model = get_model(config)
    optimizer = Adam(model.parameters(), lr=lr, weight_decay=wd, betas=(b1, b2))
    return model, optimizer
