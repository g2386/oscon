import torch
import numpy as np
from sklearn.metrics import confusion_matrix

from shared.metrics import print_mpc, metrics_per_class


def early_stop(model, data, optimizer, criterion, max_epochs, n_class, patience):
    train, valid, test = data
    classes = [i for i in range(n_class)]

    train_loss = []
    valid_loss = []

    loss = _loss(model, train, criterion)

    j, best_loss, e = 0, loss, 0
    for e in range(max_epochs):
        _train(model, train, optimizer, criterion)

        loss = _loss(model, train, criterion).item()
        train_loss.append(loss)

        loss = _loss(model, valid, criterion).item()
        valid_loss.append(loss)

        if e % 10 == 0:
            loss, cm, mpc = _evaluate(model, valid, criterion, classes)
            print(f"epoch {e} metrics:   ", end='')
            print(f"loss: {loss}   ", end='')
            print_mpc(cm, mpc, classes)

        if best_loss is None or loss < best_loss:
            j = 0
            best_loss = loss
        else:
            j += 1

        if j >= patience:
            print(f"early stopping at epoch {e + 1}\n")
            break

    test_metrics = _evaluate(model, test, criterion, classes)
    return model, (train_loss, valid_loss), test_metrics


def train_loop(model, data, optimizer, criterion, n_class, epochs=200):
    train, test = data
    classes = [i for i in range(n_class)]
    for epoch in range(1, epochs+1):
        avg_train_loss = train(model, train, optimizer, criterion)
        if epoch % 10 == 0:
            print(f"epoch {epoch}:")
            print(f"\tavg train loss: {avg_train_loss}")
            avg_test_loss, cm, mpc = _evaluate(model, test, criterion)
            print(f"\tavg test loss: {avg_test_loss}")
            print_mpc(cm, mpc, classes)


def _train(model, data, optimizer, criterion):
    model.train()
    avg_loss, i = 0, 0
    for i, batch in enumerate(data):
        model.zero_grad()
        optimizer.zero_grad()

        logits = model(batch)
        loss = criterion(logits, batch[1])
        avg_loss += loss

        loss.backward()
        optimizer.step()

    return avg_loss / (i+1)


def _evaluate(model, data, criterion, classes):
    model.eval()
    with torch.no_grad():
        cm = np.zeros((len(classes), len(classes)))
        loss_total, batch_num = 0, 0
        for batch_num, batch in enumerate(data):
            logits = model(batch)
            loss_total += criterion(logits, batch[1])
            y_true = torch.argmax(batch[1], dim=1).cpu()
            y_pred = torch.argmax(logits, dim=1).cpu()
            cm += confusion_matrix(y_true, y_pred, labels=classes)
    mpc = metrics_per_class(cm)
    avg_loss = loss_total / (batch_num + 1)
    return avg_loss, cm, mpc


def _loss(model, data, criterion):
    model.eval()
    with torch.no_grad():
        loss_total, batch_num = 0, 0
        for batch_num, batch in enumerate(data):
            logits = model.forward(batch)
            loss_total += criterion(logits, batch[1])
    return loss_total / (batch_num + 1)
