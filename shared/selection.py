import torch
import matplotlib.pyplot as plt

from itertools import product

from shared.util import parse_config
from shared.training import early_stop


def config_generator(configs):
    keys = list(configs.keys())
    values = [configs[k] for k in keys]
    products = product(*values)
    for p in products:
        config = {}
        for i, val in enumerate(p):
            key = keys[i]
            config[key] = val
        yield config


def stringify_config(config, reg_keys, arch_keys):
    elems = []
    for key in arch_keys:
        elems.append('x'.join([str(d) for d in config[key]]))
    for key in reg_keys:
        elems.append(str(config[key]))
    return '_'.join(elems)


def run_experiment(config, data, n_class, wrap_hook, max_epochs=500):
    patience = config.get('pat', 5)
    model, optimizer = parse_config(config)
    criterion = torch.nn.CrossEntropyLoss()
    dl_train, dl_valid, dl_test = [wrap_hook(d, bs=config['bs']) for d in data]

    model, losses, metrics = early_stop(model, (dl_train, dl_valid, dl_test), optimizer, criterion,
                                        max_epochs=max_epochs, n_class=n_class, patience=patience)
    return model, losses, metrics


def save_run(losses, metrics, path, model, run_no=0):
    train_loss, valid_loss = losses
    test_loss, test_cm, test_mpc = metrics
    train_loss_str = " ".join([str(loss) for loss in train_loss])
    valid_loss_str = " ".join([str(loss) for loss in valid_loss])
    metric_lines = [" ".join([str(metric) for metric in class_metrics]) for class_metrics in test_mpc]
    cm_lines = [" ".join([str(n) for n in cm_line]) for cm_line in test_cm]

    with open(f"{path}/losses_{run_no}.txt", 'w') as f:
        f.write(str(test_loss.item()) + "\n")
        f.write(train_loss_str + "\n")
        f.write(valid_loss_str + "\n")

    with open(f"{path}/metrics_{run_no}.txt", 'w') as f:
        f.write("\n".join(metric_lines))

    with open(f"{path}/cm_{run_no}.txt", 'w') as f:
        f.write("\n".join(cm_lines))

    state_dict = model.state_dict()
    torch.save(state_dict, f"{path}/params.pt")

    fig, ax = plt.subplots(figsize=(8, 6))
    ax.plot(train_loss, color='yellow', label='train set')
    ax.plot(valid_loss, color='blue', label='validation set')
    ax.set_xlim([0, len(train_loss) + int(0.2*len(train_loss))])
    ax.legend()
    fig.savefig(f"{path}/loss_{run_no}.png")
    plt.cla()
    plt.clf()
