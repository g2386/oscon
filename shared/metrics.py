import torch
import numpy as np


def conf_matrix(logits, y):
    cm = torch.zeros((logits.size(1), logits.size(1)))
    idx_preds = torch.argmax(logits, dim=1)
    idx_true = torch.argmax(y, dim=1)

    for i in range(len(idx_preds)):
        yo, yh = idx_preds[i], idx_true[i]
        cm[yo, yh] += 1

    return cm


def metrics_per_class(cm):
    total = np.sum(cm)
    metrics = []
    for i in range(cm.shape[0]):
        tp = cm[i, i]
        fp = np.sum(cm[:, i]) - tp
        fn = np.sum(cm[i, :]) - tp
        tn = total - (tp + fp + fn)

        a, p, r = (tp+tn)/total, tp/(tp+fp), tp/(tp+fn)
        f1 = 2*p*r / (p+r)
        metrics.append((a, p, r, f1))
    return metrics


def print_mpc(cm, mpc, classes):
    print("\tconf matrix:")
    print(cm)
    for i, gesture in enumerate(classes):
        print(f"\t{gesture}:   ACC {mpc[i][0]}   PRC {mpc[i][1]}   REC {mpc[i][2]}   F1S {mpc[i][3]}")
