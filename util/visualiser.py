import os
import json
from time import sleep
from argparse import ArgumentParser

import cv2
import mediapipe as mp
import numpy as np

STATIC_GESTURES = ["PTR_OPEN", "PTR_CLOSED", "SCR_APART", "SCR_TOGETHER", "STP_OVER", "STP_NEXT", "STP_UNDER"]
DYNAMIC_GESTURES = ["PRESS", "RELEASE", "CLICK", "DOUBLE", "RIGHT", "ZIN", "ZOUT"]


class Landmark:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def HasField(self, ignore):
        return False


class Frame:
    def __init__(self, frame):
        self.landmark = [Landmark(lm["x"], lm["y"], lm["z"]) for lm in normalized(frame)]


def normalized(frame):
    xm, xM, ym, yM = frame[0]["x"], frame[0]["x"], frame[0]["y"], frame[0]["y"]
    for lm in frame:
        xm = min(xm, lm["x"])
        xM = max(xM, lm["x"])
        ym = min(ym, lm["y"])
        yM = max(yM, lm["y"])

    xr = xM - xm
    yr = yM - ym
    for i, lm in enumerate(frame):
        frame[i]["x"] = (frame[i]["x"] - xm) / xr
        frame[i]["y"] = (frame[i]["y"] - ym) / yr

    return frame


def get_files(src, round, gesture, static, dynamic):
    files = os.listdir(src)
    if static and not dynamic:
        files = filter(lambda f: len(f.split("_")) == 4, files)
    elif not static and dynamic:
        files = filter(lambda f: len(f.split("_")) == 3, files)

    if round is not None:
        files = filter(lambda f: f[:-5].split("_")[-1] == str(round), files)
    if gesture is not None:
        if "_" in gesture:
            files = filter(lambda f: f.split("_")[1]+"_"+f.split("_")[2] == gesture, files)
        else:
            files = filter(lambda f: f.split("_")[1] == gesture, files)
    return list(files)


def main(args):
    mp_drawing = mp.solutions.drawing_utils
    mp_drawing_styles = mp.solutions.drawing_styles
    mp_hands = mp.solutions.hands

    file_list = get_files(args.source, args.round, args.gesture, args.static, args.dynamic)
    file_idx, frame_idx = 0, 0
    landmarks = json.load(open(f"{args.source}/{file_list[file_idx]}"))

    while True:
        hand_landmarks = Frame(landmarks[frame_idx])
        image = np.zeros((640, 480, 3), dtype=np.uint8)
        image[:] = 255

        # Draw the hand annotations on the image.
        image.flags.writeable = True

        mp_drawing.draw_landmarks(
            image,
            hand_landmarks,
            mp_hands.HAND_CONNECTIONS,
            mp_drawing_styles.get_default_hand_landmarks_style(),
            mp_drawing_styles.get_default_hand_connections_style()
        )

        cv2.imshow(file_list[file_idx], image)
        frame_idx += 1
        if frame_idx >= len(landmarks):
            frame_idx = 0
            sleep(0.45)

        key = cv2.waitKey(50)
        if key == 27:
            break
        if key == ord(','):
            new_file_index = file_idx - 1
            if new_file_index < 0:
                new_file_index = len(file_list) - 1
            frame_idx = 0
            cv2.destroyWindow(file_list[file_idx])
            file_idx = new_file_index
            landmarks = json.load(open(f"{args.source}/{file_list[file_idx]}"))
        if key == ord('.'):
            new_file_index = file_idx + 1
            if new_file_index >= len(file_list):
                new_file_index = 0
            frame_idx = 0
            cv2.destroyWindow(file_list[file_idx])
            file_idx = new_file_index
            landmarks = json.load(open(f"{args.source}/{file_list[file_idx]}"))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("source", type=str, help="path to directory with JSON examples")
    parser.add_argument("-r", "--round", type=int, choices=[0, 1, 2, 3, 4, 5],
                        help="browse only examples from given round")
    parser.add_argument("-g", "--gesture", type=str, choices=STATIC_GESTURES + DYNAMIC_GESTURES,
                        help="browse only gestures of given class")
    parser.add_argument("-s", "--static", action="store_true",
                        help="browse only static gestures")
    parser.add_argument("-d", "--dynamic", action="store_true",
                        help="browse only dynamic gestures")
    args = parser.parse_args()
    main(args)
