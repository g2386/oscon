import os
import pandas as pd
from sklearn.model_selection import train_test_split

from argparse import ArgumentParser

POSE = 'pose'
GEST = 'gesture'

POSE_CLS = ['PTR', 'SCR', 'STP']
GEST_CLS = ['PRESS', 'RELEASE', 'CLICK', 'DOUBLE', 'RIGHT', 'ZIN', 'ZOUT', 'NONE']


def split(df, classes, rvalid, rtest):
    train, valid, test = None, None, None
    split_1 = rvalid+rtest
    split_2 = rtest / split_1
    print(split_1, split_2)
    for cls in classes:
        subset = df[df['gesture'] == cls]
        sub_train, sub_test = train_test_split(subset, test_size=split_1)
        sub_valid, sub_test = train_test_split(sub_test, test_size=split_2)
        if train is None or test is None or valid is None:
            train = sub_train
            test = sub_test
            valid = sub_valid
        else:
            train = pd.concat([train, sub_train])
            test = pd.concat([test, sub_test])
            valid = pd.concat([valid, sub_valid])
    return train, valid, test


def main(args):
    train_dest = f"{args.source[:-4]}-train.csv"
    valid_dest = f"{args.source[:-4]}-valid.csv"
    test_dest = f"{args.source[:-4]}-test.csv"
    classes = POSE_CLS if args.variant == POSE else GEST_CLS

    df = pd.read_csv(args.source)
    train, valid, test = split(df, classes, rvalid=args.valid_ratio, rtest=args.test_ratio)

    train.to_csv(os.path.join(args.dest, train_dest), index=False)
    valid.to_csv(os.path.join(args.dest, valid_dest), index=False)
    test.to_csv(os.path.join(args.dest, test_dest), index=False)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('variant', type=str, help="gesture variant to split", choices=[POSE, GEST])
    parser.add_argument('source', type=str, help="source CSV file")
    parser.add_argument('dest', type=str, help="destination folder")
    parser.add_argument('-v', '--valid_ratio', type=float, default=0.1724137931034483)
    parser.add_argument('-t', '--test_ratio', type=float, default=0.1724137931034483)
    args = parser.parse_args()
    main(args)
